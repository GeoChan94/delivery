<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'main/index';
// user
$route['categoria/'] = 'Comercio_has_categoria/index';
$route['Categoria/'] = 'Comercio_has_categoria/index';
$route['categoria'] = 'Comercio_has_categoria/index';
$route['Categoria'] = 'Comercio_has_categoria/index';
$route['categoria/index'] = 'Comercio_has_categoria/index';
$route['Categoria/index'] = 'Comercio_has_categoria/index';

$route['categoria/add/'] = 'Comercio_has_categoria/add';
$route['Categoria/add/'] = 'Comercio_has_categoria/add';
$route['categoria/add'] = 'Comercio_has_categoria/add';
$route['Categoria/add'] = 'Comercio_has_categoria/add';



$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
