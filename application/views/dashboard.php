<div class="">
	<div class="col-md-12"	>
	<p class="text-gray-900">Bienvenido a <b>PUNO DELIVERY</b>, para empezar a publicar tu negocio en nuestra plataforma debes seguir los siguientes pasos:</p>	
		<ul class="list-unstyled">
			<li class="media">
				<!-- <img src="..." class="mr-3 text-gray-900 font-weight-bold" alt="..."> -->
				<h4><span class="text-danger font-weight-bold">PASO 1: </span></h4>
				<div class="media-body">
					<h4 class="mt-0 mb-1 text-gray-900 font-weight-bold">Crear negocio</h4>
					
					<p>
						Rellene datos del negocio 
					</p> 
					<ul class="list-group list-group-flush">
					  <li class="list-group-item">Nombre de negocio</li>
					  <li class="list-group-item">Descripción</li>
					  <li class="list-group-item">Imagen portada max(300x200) pixeles</li>
					  <li class="list-group-item">Lugar donde esta ubicado el negocio</li>
					  <!-- <li class="list-group-item">Vestibulum at eros</li> -->
					</ul>

					<a href="<?=base_url();?>/comercio" title="Registrar comercio">Registrar Negocio click aqui</a>
				</div>
			</li>
			<li class="media my-4">
				<h4><span class="text-danger font-weight-bold">PASO 2: </span></h4>
				<div class="media-body">
					<h5 class="mt-0 mb-1 text-gray-900 font-weight-bold">Asociar categorias al negocio</h5>
					Asocie categorias relacionadas con su negocio <a href="<?=base_url();?>/categoria" title="Registrar comercio">Click aqui</a>
				</div>
			</li>
		</ul>
	</div>
	<!-- <h2>Please navigate to the appropriate controller / action to open the associated function with your generated code.</h2> -->
</div>