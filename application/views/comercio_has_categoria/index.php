 <div class="col-md-12 text-center font-weight-bold text-uppercase">
 	<h4>Lista de categorias asociadas</h4>
 </div>
 <?php if (!empty($error)) {?>
 	<div class="col-md-12 alert alert-danger text-center">
 		<?=$error;?><br>
 		<a href="<?=base_url()?>comercio" title=""class="btn btn-danger">Crear Comercio</a>
 	</div>
 <?php }else{ ?>
 	<?php if ($comercio_has_categorias==NULL){ ?>
 		<div class="float-right">
 			<a href="<?=base_url();?>categoria/add" title="" class="btn btn-primary">Agregar Categoria</a>
 		</div>
 		
 	<?php }else{?>

 		<div class="float-right">
 			<a href="<?php echo site_url('categoria/add'); ?>" class="btn btn-success">Agregar</a> 
 		</div>

 		<table class="table table-striped table-bordered">
 			<tr>
 				<th>#</th>
 				<th>Categoria</th>
 				<th>Acción</th>
 			</tr>
 			<?php foreach($comercio_has_categorias as $c){ ?>
 				<tr>
 					<td><?php echo $c['idcategoria']; ?></td>
 					<td><?php echo $c['nombre']; ?></td>
 					<td>
 						<!-- <a href="<?php echo site_url('comercio_has_categorium/edit/'.$c['idcomercio']); ?>" class="btn btn-info btn-xs">Edit</a>  -->
 						<a href="<?php echo site_url('comercio_has_categoria/eliminar_comercia_has_categoria/'.$c['idcategoria']); ?>" class="btn btn-danger btn-xs">Delete</a>
 					</td>
 				</tr>
 			<?php } ?>
 		</table>


 	<?php } 
 }
 ?>
