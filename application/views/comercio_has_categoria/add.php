

<form action="" method="post" enctype="multipart/form-data">	
	<table class="table table-striped table-bordered">
	    <tr>
			<th>Idcomercio</th>
			<th>Idcategoria</th>
			<th>Actions</th>
	    </tr>
		<?php foreach($categorias as $c){ ?>
	    <tr>
			<td><?php echo $c['idcategoria']; ?></td>
			<td><?php echo $c['nombre']; ?></td>
			<td>
	            <a href="<?php echo site_url('comercio_has_categoria/agregar_categoria_has_cocomercio/'.$c['idcategoria']); ?>" class="btn btn-info btn-xs">Agregar</a> 
	        </td>
	    </tr>
		<?php } ?>
	</table>
</form>
<!-- <?php var_dump($categorias); ?> -->
