<div class="pull-right">
	<a href="<?php echo site_url('comercio_has_producto/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idcomercio</th>
		<th>Idproducto</th>
		<th>Actions</th>
    </tr>
	<?php foreach($comercio_has_producto as $c){ ?>
    <tr>
		<td><?php echo $c['idcomercio']; ?></td>
		<td><?php echo $c['idproducto']; ?></td>
		<td>
            <a href="<?php echo site_url('comercio_has_producto/edit/'.$c['idcomercio']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('comercio_has_producto/remove/'.$c['idcomercio']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
