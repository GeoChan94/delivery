
<div class="container">
	<h1 class="text-center">INGRESA TUS DATOS DE COMERCIO</h1>
 <?php echo form_open_multipart('Comercio/add');?>
	<form action="" method="post" enctype="multipart/form-data">
		<div class="row form-group ">



			<div class="col-md-6">
				<div class="form-group">
					<label for="nombreComercio">Nombre del Negocio <span class="text-danger">*</span></label>
					<input type="text" name="nombreComercio" id="nombreComercio" class="form-control temp_text" value="" placeholder="Nombre del negocio">
				</div>
				<div class="form-group">
					<label for="ciudad">Ubicación del negocio <span class="text-danger">*</span> </label>
					<select name="iddistrito" class="form-control">
						<!-- <option value="" select>PUNO</option> -->
						<?php foreach ($distritos as $key => $value): ?>
							<option value="<?=$value['iddistrito'];?>" select><?=$value['nombre'];?></option>
						<?php endforeach ?>
					</select>
					<!-- <input type="text" name="ciudad" id="ciudad" class="form-control temp_text" value="" placeholder="ciudad"> -->
				</div>
				<div class="form-group">
					<label for="descripcionComercio">Descripcion del Negocio <span class="text-danger">*</span> </label>
					<textarea name="descripcionComercio" id="descripcionComercio" class="form-control temp_text" rows="7" placeholder="descripcion del negocio"></textarea>
				</div>
				<div class="form-group">
					<label for="urlImagen">Imagen de Portada <span class="text-danger">max(300x200 pixels)*</span></label>
				<div class="input-group">
					<!-- <input type="file" name="userfile"> -->
					<input type="text" class="form-control"  name="urlImagen" id="urlImagen"readonly>
					<span class="input-group-btn">
						<span class="btn btn-primary btn-file">
							<span class="fa fa-upload"></span> Subir<input name="urlImagen" type="file" id="imgInp">
						</span>
					</span>
				</div>
				</div>
				
			</div>
			<div class="col-md-4 offset-1">
				<label for="urlImagen">Previsualizacion de Portada </label>
				<div class="card">
                                <img class="card-img-top card-img-custom img-fluid" src="" alt="Card image cap" id='img-upload'>
                                <div class="card-body">
                                    <h5 class="card-title nombreComercio">Nombre Negocio</h5>
                                    <p class="card-text descripcionComercio">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum atque, commodi aperiam voluptas a eius dolor. Ipsa, odio iusto voluptatem commodi praesentium culpa numquam repellat aliquam magni facilis voluptatum ex.</p>
                                        <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                                </div>
                            </div>
			</div>
		</div>
		<hr>
		<div class="row form-group">

			<div class="col-md-6">
				<label for="ruc">RUC (Opcional) </label>
				<input type="text" name="ruc" id="ruc" class="form-control">
			</div>
			<div class="col-md-6">
				<label for="direccionEmpresa">Direccion del Negocio (Opcional)</label>
				<input type="text" name="direccionEmpresa" id="direccionEmpresa" class="form-control">
			</div>
			
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label for="celular">Celular (Opcional)</label>
				<input type="text" name="celular" id="celular" class="form-control">
			</div>
			<div class="col-md-6">
				<label for="correo">Correo (Opcional)</label>
				<input type="text" name="correo" id="correo" class="form-control">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label for="urlFacebook">URL facebook (Opcional)</label>
				<input type="text" name="urlFacebook" id="urlFacebook" class="form-control">
			</div>
			<div class="col-md-6">
				<label for="urlPaginaWeb">URL Pagina Web (Opcional)</label>
				<input type="text" name="urlPaginaWeb" id="urlPaginaWeb" class="form-control">
			</div>
		</div>


	</div>
	<div class="form-group">
		<div class="col-md-12 text-center">
			<button type="submit" class="btn btn-success">Crear Comercio</button>
		</div>
	</div>
