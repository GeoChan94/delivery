
<div class="container">
	<h2 class="font-weight-bold text-center text-uppercase">ACTUALIZAR DATOS DE COMERCIO</h2>
	<hr>
 <?php echo form_open_multipart('Comercio/edit');?>
	<form action="" method="post" enctype="multipart/form-data">
		<div class="row form-group ">



			<div class="col-md-6">
				<div class="form-group">
					<label for="nombreComercio">Nombre del Negocio <span class="text-danger">*</span></label>
					<input type="text" name="nombreComercio" id="nombreComercio" class="form-control temp_text" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $comercio['nombre']); ?>">
				</div>
				<div class="form-group">
					<label for="ciudad">Ubicación del negocio <span class="text-danger">*</span> </label>
					<select name="iddistrito" class="form-control">
						<option value="<?=$comercio['distrito']['iddistrito'];?>" select ><?=$comercio['distrito']['nombre'];?></option>
						<?php foreach ($distritos as $key => $value): ?>
							<option value="<?=$value['iddistrito'];?>" ><?=$value['nombre'];?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="form-group">
					<label for="descripcionComercio">Descripcion del Negocio <span class="text-danger">*</span> </label>
					<textarea name="descripcionComercio" id="descripcionComercio" class="form-control temp_text" rows="7" ><?php echo ($this->input->post('descripcion') ? $this->input->post('descripcion') : $comercio['descripcion']); ?></textarea>
				</div>
				<div class="form-group">
					<label for="urlImagen">Imagen de Portada <span class="text-danger">max(300x200 pixels)*</span></label>
				<div class="input-group">
					<!-- <input type="file" name="userfile"> -->
					<input type="text" class="form-control"  name="urlImagen" id="urlImagen" readonly value="<?php echo ($this->input->post('urlimagen') ? $this->input->post('urlimagen') : $comercio['urlimagen']); ?>" >
					<span class="input-group-btn">
						<span class="btn btn-primary btn-file">
							<span class="fa fa-upload"></span> Subir<input name="urlImagen" type="file" id="imgInp" value="<?php echo ($this->input->post('urlimagen') ? $this->input->post('urlimagen') : $comercio['urlimagen']); ?>" >
						</span>
					</span>
				</div>
				</div>
				
			</div>
			<div class="col-md-4 offset-1">
				<label for="urlImagen">Previsualizacion de Portada </label>
				<div class="card">
					<img class="card-img-top card-img-custom" src="<?=base_url().'galeria/'.($comercio['urlimagen']);?>"  alt="Card image cap" id='img-upload'>
					<div class="card-body">
						<h5 class="card-title nombreComercio"><?=$comercio['nombre'];?></h5>
						<p class="card-text descripcionComercio"><?=$comercio['descripcion'];?></p>
						<!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row form-group">

			<div class="col-md-6">
				<label for="ruc">RUC (Opcional) </label>
				<input type="text" name="ruc" id="ruc" class="form-control" value="<?php echo ($this->input->post('ruc') ? $this->input->post('ruc') : $comercio['ruc']); ?>">
			</div>
			<div class="col-md-6">
				<label for="direccionEmpresa">Direccion del Negocio (Opcional)</label>
				<input type="text" name="direccionEmpresa" id="direccionEmpresa" class="form-control" value="<?php echo ($this->input->post('direccion') ? $this->input->post('direccion') : $comercio['direccion']); ?>">
			</div>
			
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label for="celular">Celular (Opcional)</label>
				<input type="text" name="celular" id="celular" class="form-control" value="<?php echo ($this->input->post('celular') ? $this->input->post('celular') : $comercio['celular']); ?>">
			</div>
			<div class="col-md-6">
				<label for="correo">Correo (Opcional)</label>
				<input type="text" name="correo" id="correo" class="form-control" value="<?php echo ($this->input->post('correo') ? $this->input->post('correo') : $comercio['correo']); ?>">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label for="urlFacebook">URL facebook (Opcional)</label>
				<input type="text" name="urlFacebook" id="urlFacebook" class="form-control" value="<?php echo ($this->input->post('urlfacebook') ? $this->input->post('urlfacebook') : $comercio['urlfacebook']); ?>">
			</div>
			<div class="col-md-6">
				<label for="urlPaginaWeb">URL Pagina Web (Opcional)</label>
				<input type="text" name="urlPaginaWeb" id="urlPaginaWeb" class="form-control" value="<?php echo ($this->input->post('urlpaginaweb') ? $this->input->post('urlpaginaweb') : $comercio['urlpaginaweb']); ?>">
			</div>
		</div>
		<div class="row form-group">
		<div class="col-md-12 text-center">
			<hr>
			<button type="submit" class="btn btn-success btn-block">Actualizar</button>
		</div>
	</div>

	</div>
	
