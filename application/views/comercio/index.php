 <?php if ($comercio==NULL){ ?>
 		<a href="<?=base_url();?>comercio/add" title="" class="btn btn-danger btn-block"><i class="fa fa-building"></i> Crear Comercio</a>
 <?php }else{?>

<div class="row form-group ">
		<div class="col-md-12 ">
			<h2 class="font-weight-bold text-center text-uppercase">datos de comercio</h2>
		</div>
		
		<div class="col-md-12">
		<a href="<?=base_url();?>comercio/edit" class="btn btn-block btn-warning">Editar</a>
		<hr>
		</div>

			<div class="col-md-6">
				<div class="form-group">
					<label for="nombreComercio">Nombre del Negocio <span class="text-danger">*</span></label>
					<input type="text" name="nombreComercio" id="nombreComercio" class="form-control" value="<?=$comercio['nombre'];?>"  readonly="">
				</div>
				<div class="form-group">
					<label for="ciudad">Ubicación del negocio <span class="text-danger">*</span> </label>
					<input type="text" name="iddistrito" id="iddistrito" class="form-control" value="<?=$comercio['distrito']['nombre'];?>"   readonly="">
				</div>
				<div class="form-group">
					<label for="descripcionComercio">Descripcion del Negocio <span class="text-danger">*</span> </label>
					<textarea name="descripcionComercio" id="descripcionComercio" class="form-control" rows="7" readonly><?=$comercio['descripcion'];?> </textarea>
				</div>
				<div class="form-group">
					<label for="urlImagen">Imagen de Portada <span class="text-danger">*</span></label>
				<div class="input-group">
		
					<input type="text" class="form-control" value="<?=$comercio['urlimagen'];?>"  readonly>
					<!-- <span class="input-group-btn">
						<span class="btn btn-primary btn-file">
							<span class="fa fa-upload"></span> Subir<input name="urlImagen" type="file" id="imgInp" >
						</span>
					</span> -->
				</div>
				</div>
				
			</div>
			<div class="col-md-4 offset-1">
				<label for="urlImagen">Previsualizacion de Portada </label>
				<div class="card">
					<img  class="card-img-top card-img-custom" src="<?=base_url();?>galeria/<?=$comercio['urlimagen'];?>" alt="">
					<div class="card-body">
						<h5 class="card-title"><?=$comercio['nombre'];?></h5>
						<p class="card-text"><?=$comercio['descripcion'];?></p>
						<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row form-group">

			<div class="col-md-6">
				<label for="ruc">RUC (Opcional) </label>
				<input type="text" name="ruc" id="ruc" class="form-control" value="<?=$comercio['ruc'];?>"  readonly="">
			</div>
			<div class="col-md-6">
				<label for="direccionEmpresa">Direccion del Negocio (Opcional)</label>
				<input type="text" name="direccionEmpresa" id="direccionEmpresa" class="form-control" value="<?=$comercio['direccion'];?>"   readonly="">
			</div>
			
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label for="celular">Celular (Opcional)</label>
				<input type="text" name="celular" id="celular" class="form-control" value="<?=$comercio['celular'];?>"  readonly="">
			</div>
			<div class="col-md-6">
				<label for="correo">Correo (Opcional)</label>
				<input type="text" name="correo" id="correo" class="form-control"  value="<?=$comercio['correo'];?>"  readonly="">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label for="urlFacebook">URL facebook (Opcional)</label>
				<input type="text" name="urlFacebook" id="urlFacebook" class="form-control"  value="<?=$comercio['urlfacebook'];?>"  readonly="">
			</div>
			<div class="col-md-6">
				<label for="urlPaginaWeb">URL Pagina Web (Opcional)</label>
				<input type="text" name="urlPaginaWeb" id="urlPaginaWeb" class="form-control"  value="<?=$comercio['urlpaginaweb'];?>"  readonly="">
			</div>
		</div>
	<?php } ?>
