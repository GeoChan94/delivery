<!DOCTYPE html>
<html>

<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS styles -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /> -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->


  <!-- JS Libs -->
	    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js" type="text/javascript"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script> -->
       <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

       <?php
       $this->load->view('template/css');
       ?>
     </head>

     <body>
       <!-- Page Wrapper -->
       <div id="wrapper">
         <?php	

         if(isset($_view) && $_view)
		// $this->load->view('template/admin/sidebar');

           ?>
         <!-- Content Wrapper -->

         <div id="content-wrapper" class="d-flex flex-column" style="min-height: 100vh;">

          <!-- Main Content -->
          <div id="content">

            <?php $this->load->view('template/main/navbar');?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

             <?php $this->load->view($_view); ?>
             <!-- Page Heading -->



           </div>
           <!-- /.container-fluid -->

         </div>
         <!-- End of Main Content -->

         <?php $this->load->view('template/main/footer');?>

       </div>
       <!-- End of Content Wrapper -->
     </div>
     <!-- End of Page Wrapper -->
     <?php
     $this->load->view('template/js');
	  //condidional para cargar los js propios si es que fueran necesarios

     ?>
     <script type="text/javascript" charset="utf-8" async defer>
      var temp_iddistrito='';
        var temp_idcategoria='';
        var all_comercios = jQuery.parseJSON('<?=json_encode(@$comercio); ?>');
        var all_categorias = jQuery.parseJSON('<?=json_encode(@$categorias); ?>');
        var all_distritos = jQuery.parseJSON('<?=json_encode(@$distritos); ?>');

        var categoria_selected = null;
      var distrito_selected = null;
      var filtrados = [];

      function imprimir_comercios(idcategoria,iddistrito){
        if(idcategoria == 'reset') categoria_selected = null;
        else if(idcategoria) categoria_selected = idcategoria;

        if(iddistrito == 'reset')distrito_selected = null;
        else if(iddistrito) distrito_selected = iddistrito;

        var  resto=0;
        comercio_row='<div class="row">'
        comercio_body='';
        categorias='';

        filtrados = all_comercios;
        console.log('mostrando con: ',categoria_selected,distrito_selected);
        if(categoria_selected){
            filtrados = filtrados.filter(function(comercio){
                return comercio.categorias.filter(function(cat){
                    return cat.idcategoria==idcategoria;
                }).length != 0;
            });
        }
        if(distrito_selected){
            filtrados = filtrados.filter(function(comercio){
                return comercio.iddistrito==iddistrito;
            });
        }
        filtrados.forEach(function(comercio,index){
          // console.log(index);
          
          comercio.categorias.forEach(function(categoria){
            categorias+='<small class="border rounded border-secondary bg-secondary text-white">'+categoria.nombre+'</small> ';
          });

          resto = index % 3;
          if (index==0) {
            comercio_body='<div class="row mb-3">';
          }else if(resto==0){
            console.log('entra');
            comercio_body+='</div><div class="row mb-3">';
          }else{
            console.log('no entra');
          }

          comercio_body+=''+
          '<div class=" col-md-4 item-servicio" >'+
            '<div class="card h-100">'+
              '<div class="thumbnail">'+
                '<img class="card-img-top card-img" src="<?=base_url();?>galeria/'+comercio.urlimagen+'" alt="Card image cap">'+
                '<span class="categoria">'+comercio.ciudad+'</span>'+
              '</div>'+          
              '<div class="card-body">'+
                '<h5 class="card-title text-gray-900 font-weight-bolder">'+comercio.nombrecomercio+'</h5>'+
                '<a class="text-primary font-weight-bold" href="tel:+51'+comercio.celular+'">'+(comercio.celular?'+51'+comercio.celular:"")+'</a>'+
                '<p class="text-gray-900 font-weight-light">'+comercio.correo+'</p>'+
                '<p><small class="card-text">'+comercio.descripcion+'</small></p>'+
                '<p class="text-gray-900 font-weight-bolder">Cateogria</p>'+
                '<p class="card-text"><small class="text-muted">'+categorias+'</small></p>'+
              '</div>'+
              (comercio.urlfacebook||comercio.urlpaginaweb?
                '<div class="card-footer bg-transparent p-1 text-center">'+              
               (comercio.urlfacebook?'<a class="fab fa-facebook-square " target="_blank" href="'+comercio.urlfacebook+'" title="Facebook"></a>':"")+' '+
               (comercio.urlpaginaweb?'<a class="fas fa-globe  text-gray-900" target="_blank" href="'+comercio.urlpaginaweb+'" title="Pagina Web"></a>':"")+
              '</div>'
              :"")+              
            '</div>'+
          '</div>';
          categorias='';


        });
        comercio_end_row="</div>";
        $('#comercios').html(comercio_body);
        txt_total_comercios();
        imprimir_categoria(categoria_selected);
      imprimir_distritos(distrito_selected); 

      }

        imprimir_comercios(null,null);
  
      function imprimir_categoria(id){
        if(!id){
          let tmp_html = "";
        all_categorias.filter(function(categoria){
            
            if(distrito_selected){
                return !!filtrados.filter(function(comercio){
                    return comercio.iddistrito==distrito_selected;
                }).filter(function(comercio){
                    return !!comercio.categorias.filter(function(ccc){
                        return ccc.idcategoria==categoria.idcategoria;
                    }).length;
                }).length;
            }else{
                return true;
            }
            
        }).forEach(function(categoria){
            tmp_html = tmp_html + `
                <li class="list-group-item p-1 border-0 cursor-pointer" onclick="imprimir_comercios('${categoria.idcategoria}',null)">
                
                    <small>${categoria.nombre}</small> 
                </li>
            `;
        });
        document.getElementById('categorias').innerHTML = tmp_html;
        }else{
          // var txt_categorias='';
          // idcategoria=categorias.idcategoria;
          // categorias.forEach(function(categoria){
          //   txt_categorias+=''+
          //   '<li class="list-group-item p-1 border-0" ><small class="cursor-pointer" onclick="filtrar_categoria('+categoria.idcategoria+');">'+categoria.nombre+'</small></li>';

          // });
          // $('#categorias').html(txt_categorias);

          let tmp_html = "";
        all_categorias.filter(function(categoria){
            return categoria.idcategoria==id;
        }).forEach(function(categoria){
            tmp_html = tmp_html + `
                <li class="list-group-item p-1 border-0 cursor-pointer" onclick="imprimir_comercios('reset',null)">
                    <span class="badge badge-danger float-right" id="badge-0">
                        <i class="fas fa-times"></i>
                    </span>
                    <small>${categoria.nombre}</small>
                </li>
            `;
        });
        console.log(tmp_html);
        document.getElementById('categorias').innerHTML = tmp_html;

        }
        
      }
      // imprimir_categoria(null);
function imprimir_distritos(id){
    if(!id){
        let tmp_html = "";
        all_distritos.filter(function(distrito){

            if(categoria_selected){
                return !!filtrados.filter(function(comercio){
                    return categoria_selected?!!comercio.categorias.filter(function(ccc){
                        return ccc.idcategoria==categoria_selected;
                    }).length:true;
                }).length;
            }else{
                return true;
            }

        }).forEach(function(distrito){
            tmp_html = tmp_html + `
            <li class="list-group-item p-1 border-0 cursor-pointer" onclick="imprimir_comercios(null,'${distrito.iddistrito}')">
                    <small>${distrito.nombre}</small>
                
            </li>
            `;
        });
        document.getElementById('distritos').innerHTML = tmp_html;
    }else{
        let tmp_html = "";
        all_distritos.filter(function(distrito){
            return distrito.iddistrito==id;
        }).forEach(function(distrito){
            tmp_html = tmp_html + `
            <li class="list-group-item p-1 border-0 cursor-pointer" onclick="imprimir_comercios(null,'reset')">
                <span class="badge badge-danger float-right" id="badge-0">
                        <i class="fas fa-times"></i>
                    </span>
                    <small>${distrito.nombre}</small>
                
            </li>
            `;
        });
        document.getElementById('distritos').innerHTML = tmp_html;
    }
}
function txt_total_comercios(){
  
  $('#total_comercio').html('Se encontraron ('+filtrados.length+') Resultados');

}

    </script>
  </body>
<style type="text/css" media="screen">
 .thumbnail .categoria {
    position: absolute;
    top: 10px;
    left: 15px;
    background-color: #bd00ff;
    color: white;
    padding: 0px 5px 0px 5px;
    border-radius: 5px 5px;
    text-transform: capitalize;
}
/*
*/
.card-img{
  max-height: 200px;
}
.cursor-pointer{
  cursor: pointer;
}
</style>
  </html>
