<!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url();?>dashboard">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-book"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin Puno Delivery</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Administracion
      </div>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('comercio');?>">
          <i class="fas fa-fw fa-table"></i>
          <span>Comercio</span></a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('categoria');?>">
          <i class="fas fa-fw fa-table"></i>
          <span>Categorias</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url('producto');?>">
          <i class="fas fa-fw fa-table"></i>
          <span>Productos</span></a>
      </li>
      <hr class="sidebar-divider">
      


      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
