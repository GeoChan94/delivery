<form action="<?=base_url();?>registrar/add" method="post" accept-charset="utf-8">
	<div class="row bg-light pt-4 pb-4">
		<div class="col-12">

			<div class="h4 font-weight-bold text-uppercase text-center mb-3 mt-3">
				CREAR UN USUARIO
			</div>
		</div>
		<div class="col-10 offset-1 col-md-10 offset-md-1">
			<div class="form-group">
				<label for="nombrecompleto">Nombre Completo</label>
				<input type="text" class="form-control" name="nombrecompleto" id="nombrecompleto" aria-describedby="nombrecompleto" placeholder="Nombre Completo" value="">
			</div>
			<div class="form-group">
				<label for="dni">DNI</label>
				<input type="text" class="form-control" name="dni" id="dni" aria-describedby="dni" placeholder="DNI" value="">
			</div>
			<div class="form-group">
				<label for="usuario">Usuario</label>
				<input type="text" class="form-control" name="usuario" id="usuario" aria-describedby="usuario" placeholder="usuario" value="">
			</div>
			<div class="form-group">
				<label for="password">Constraseña</label>
				<input type="password" class="form-control" name="password" id="password" aria-describedby="password" placeholder="Password" value="">
			</div>
			<?php echo @$error;?>
			
			<div class="row">
				<!-- <button type="submit" class="btn btn-info">Crear Cuenta</button> -->
				<div class="col-12">
					<div class="text-center mb-3">
						<input type="submit" value="Crear Cuenta" class="btn btn-info  text-uppercase w-100">
						<!-- <span class="btn btn-primary  text-uppercase w-100">ENTRAR</span> -->
					</div>
				</div>
			</div>
			<div class="text-center">
				<a href="<?=base_url();?>login" title=""><small>Ya tengo una cuenta!</small></a><br>	
				<a href="<?=base_url();?>" title=""><small>Regresar a <?=base_url();?></small></a>	
			</div>


		</div>

	</div>
</form>
