
    <section class="row titlePag" id="tituloPag">
        <div class="col-md-12">
            <h1>Lista de Negocios</h1>
        </div>
    </section>
    <div class="row">
        <section class="col-md-2 col-sm-12 filtros">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="card">
                        <div class="card-header p-0">
                            <h5 class="mb-0">
                                <button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#listCategorias" aria-expanded="false" aria-controls="listCategorias">
                                    <i class="fa fa-layer-group"></i>
                                    <small>CATEGORIAS</small> 
                                </button>
                            </h5>
                        </div>
                        <div id="listCategorias" class="collapse show">
                            <div class="card card-body p-2">
                                <ul class="list-group" id="categorias">
                                    <li class="list-group-item">item 1</li>
                                    <li class="list-group-item">item 2</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 ">
                    <div class="card">
                        <div class="card-header p-0">
                            <h5 class="mb-0">
                                <button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#listDistritos" aria-expanded="false" aria-controls="listDistritos">
                                    <i class="fa fa-layer-group"></i>
                                    <small>DISTRITOS</small> 
                                </button>
                            </h5>
                        </div>
                        <div id="listDistritos" class="collapse show">
                            <div class="card card-body p-2">
                                <ul class="list-group" id="distritos">
                                    <li class="list-group-item">item 1</li>
                                    <li class="list-group-item">item 2</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-12 productos">
                    <div class="card">
                        <div class="card-header" id="card-Productos">
                            <h5 class="mb-0">
                                <button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#listProducto" aria-expanded="true" aria-controls="listProducto">
                                    <i class="fab fa-product-hunt"></i> PRODUCTOS
                                </button>
                            </h5>
                        </div>

                        <div id="listProducto" class="collapse show">
                            <div class="card-body">
                                <ul class="list-group">
                                    <li class="list-group-item">producto 1</li>
                                    <li class="list-group-item">producto 2</li>
                                    <li class="list-group-item">producto 3</li>
                                    <li class="list-group-item">producto 4</li>
                                    <li class="list-group-item">producto 5</li>
                                    <li class="list-group-item">producto 6</li>
                                    <li class="list-group-item">producto 7</li>
                                    <li class="list-group-item">producto 8</li>
                                    <li class="list-group-item">producto 9</li>
                                    <li class="list-group-item">producto 10</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </section>
        <section class="col-md-10 col-sm-12 negocios">
            <div class="row">
                <div class="col-md-12 resultados">
                    <p><b id="total_comercio"></b></p>
                </div>
                <div class="col-md servicios" id="comercios">
                    <div class="row">

                        <div class="col-md-12 text-center div-title"><h4 class="div-content-title">
                          <strong>ACTIVIDADES MÁS VENDIDAS</strong></h4>
                      </div>
                      
                      <div class="col-12 col-sm-6 col-md-3 col-xl-3">
                        <div class="targeta">
                          <div class="thumbnail"> 
                            <a href="//incalake.com/es/puno/excursion-tour-isla-flotante-de-los-uros"><img src="//incalake.com/galeria/admin/short-slider/PUNO/UROS/thumbs/uros-grupo-2.png"></a>
                            <span class="categoria">turismo cultural</span>
                        </div>
                        <div class="contenido">
                            <div class="titulo">
                              <a href="//incalake.com/es/puno/excursion-tour-isla-flotante-de-los-uros">TOUR A LA ISLA FLOTANTE DE LOS UROS (TURNO MAÑANA).</a>
                          </div>
                          <div class="duracion">
                              <i class="fa fa-clock-o"></i> <span>duración: 3 horas</span>
                              <br><i class="fa fa-clock-o"></i> <span>1 Horario Disponible</span>
                          </div>
                          <div class="fecha">
                          </div>
                      </div>
                      <div class="extra">
                        <div class="precio">
                          <span class="txt-precio-desde">Desde</span>
                          <span class="oferta">$15.00</span>

                      </div>
                      <div class="explorar">
                          <a href="//incalake.com/es/puno/excursion-tour-isla-flotante-de-los-uros">Leer Más&gt;&gt;</a>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-sm-1 col-md-6 col-lg-4 item-servicio " >
            <div class="card" style="height:100%;">
              <div class="thumbnail">
                <img class="card-img-top" src="//incalake.com/es/puno/excursion-tour-isla-flotante-de-los-uros"><img src="//incalake.com/galeria/admin/short-slider/PUNO/UROS/thumbs/uros-grupo-2.png" alt="Card image cap">
                <span class="categoria">'+comercio.ciudad+'</span>
            </div>'+        
            <div class="card-body ">
                <h5 class="card-title">'+comercio.nombrecomercio+'</h5>
                <p class="card-text">'+comercio.descripcion+'</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
    </div>


          <div class="col-12 col-sm-6 col-md-3 col-xl-3">
            <div class="targeta">
              <div class="thumbnail"> 
                <a href="//incalake.com/es/puno/isla-de-los-uros-y-taquile-lancha-rapida"><img src="//incalake.com/galeria/admin/short-slider/PUNO/TAQUILE/thumbs/23882511718_406ba4ee01_k.jpg"></a>
                <span class="categoria">turismo cultural</span>
                <!-- <span class="descuento">114</span> -->
            </div>
            <div class="contenido">
                <div class="titulo">
                  <a href="//incalake.com/es/puno/isla-de-los-uros-y-taquile-lancha-rapida">TOUR COMPARTIDO A LOS UROS Y TAQUILE EN LANCHA RÁPIDA 1 DIA</a>
              </div>
              <div class="duracion">
                  <i class="fa fa-clock-o"></i> <span>duración: 1 días</span>
                  <br><i class="fa fa-clock-o"></i> <span>1 Horario Disponible</span>
              </div>
              <div class="fecha">
                  <!--<i class="fa fa-calendar"></i> <span>Hasta 12-septiembre-2017</span>-->
              </div>
          </div>
          <div class="extra">
            <div class="precio">
              <span class="txt-precio-desde">Desde</span>
              <span class="oferta">$49.00</span>
                    <!-- <span class="oferta">$<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Undefined index: precio_oferta</p>
<p>Filename: views/mas_comprados_index.php</p>
<p>Line Number: 49</p>

</div></span> -->
<!-- <span class="normal">$49.00</span> -->
</div>
<div class="explorar">
  <a href="//incalake.com/es/puno/isla-de-los-uros-y-taquile-lancha-rapida">Leer Más&gt;&gt;</a>
</div>
</div>
</div>
</div>


<div class="col-12 col-sm-6 col-md-3 col-xl-3">
    <div class="targeta">
      <div class="thumbnail"> 
        <a href="//incalake.com/es/cusco/tour-valle-sagrado-maras-moray-1d-dia-completo"><img src="//incalake.com/galeria/admin/short-slider/cusco/VALLE-SAGRADO/thumbs/valle-sagrado-cusco-4.png"></a>
        <span class="categoria">turismo cultural</span>
        <!-- <span class="descuento">95</span> -->
    </div>
    <div class="contenido">
        <div class="titulo">
          <a href="//incalake.com/es/cusco/tour-valle-sagrado-maras-moray-1d-dia-completo">TOUR AL VALLE SAGRADO Y MARAS MORAY (SERVICIO COMPARTIDO 1D).</a>
      </div>
      <div class="duracion">
          <i class="fa fa-clock-o"></i> <span>duración: 1 días</span>
          <br><i class="fa fa-clock-o"></i> <span>1 Horario Disponible</span>
      </div>
      <div class="fecha">
          <!--<i class="fa fa-calendar"></i> <span>Hasta 12-septiembre-2017</span>-->
      </div>
  </div>
  <div class="extra">
    <div class="precio">
      <span class="txt-precio-desde">Desde</span>
      <span class="oferta">$59.00</span>
                    <!-- <span class="oferta">$<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Undefined index: precio_oferta</p>
<p>Filename: views/mas_comprados_index.php</p>
<p>Line Number: 49</p>

</div></span> -->
<!-- <span class="normal">$59.00</span> -->
</div>
<div class="explorar">
  <a href="//incalake.com/es/cusco/tour-valle-sagrado-maras-moray-1d-dia-completo">Leer Más&gt;&gt;</a>
</div>
</div>
</div>
</div>

<div class="col-12 col-sm-6 col-md-3 col-xl-3">
  <div class="targeta targeta-ver-ofertas">
    <div class="thumbnail"> 
      <a href="//incalake.com/es/cusco/tour-dia-completo-vinicunca-cusco"><img src="//incalake.com/galeria/admin/short-slider/cusco/MONTANA-COLORES-VINICUNCA/thumbs/montana-colores-vinicunca-6.png"></a>
      <span class="categoria">turismo cultural</span>
      <!-- <span class="descuento">54</span> -->
  </div>
  <div class="contenido">
      <div class="titulo">
        <a href="//incalake.com/es/cusco/tour-dia-completo-vinicunca-cusco">VINICUNCA. FULL DAY EN LA MONTAÑA ARCO IRIS.</a>
    </div>
    <div class="duracion">
      <i class="fa fa-clock-o"></i> <span>duración: 1 días</span>
      <br><i class="fa fa-clock-o"></i> <span>1 Horario Disponible</span>
  </div>
  <div class="fecha">
    <!-- <i class="fa fa-calendar"></i> <span>Hasta 12-septiembre-2017</span> -->
</div>
</div>
<div class="extra">
  <div class="precio">
    <span class="txt-precio-desde">Desde</span>
    <span class="oferta">$59.00</span>
                    <!-- <span class="oferta">$<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Undefined index: precio_oferta</p>
<p>Filename: views/mas_comprados_index.php</p>
<p>Line Number: 88</p>

</div></span> -->
<!-- <span class="normal">$59.00</span> -->
</div>
<div class="explorar">
    <a href="//incalake.com/es/cusco/tour-dia-completo-vinicunca-cusco">Leer Más&gt;&gt;</a>
</div>
</div>
<div class="div-content-ofertas">
  <div class="div-count-ofertas"><span class="num">15</span> <br> Super <br>Ventas</div>
  <div class="div_ver_ofertas"><a href="//incalake.com/es/superventas"><span>Ver Más superVentas</span></a></div>
</div>
</div>
</div>





</div>
<div class="row d-flex align-items-stretch">
    <div class="col-sm-1 col-md-6 col-lg-4 item-servicio" style="margin-bottom: 20px;">
        <div class="card">
            <div class="thumbnail">
                <img class="card-img-top" src="https://incalake.com/galeria/admin/short-slider/PUNO/PUNO-CIUDAD/parque-pino.jpg" alt="Card image cap">
                <span class="categoria">Turismo Cultural</span>
            </div>

            <div class="card-body">
                <h5 class="card-title">Nombre Negocio</h5>
                <p class="card-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Rerum atque, commodi aperiam voluptas a eius dolor. Ipsa, odio iusto voluptatem commodi praesentium culpa numquam repellat aliquam magni facilis voluptatum ex.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-1 item-servicio" style="margin-bottom: 20px;">
        <div class="card">
            <img class="card-img-top" src="https://incalake.com/galeria/admin/short-slider/PUNO/PUNO-CIUDAD/parque-pino.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-1 item-servicio" style="margin-bottom: 20px;">
        <div class="card">
            <img class="card-img-top" src="https://incalake.com/galeria/admin/short-slider/PUNO/PUNO-CIUDAD/parque-pino.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-1 item-servicio" style="margin-bottom: 20px;">
        <div class="card">
            <img class="card-img-top" src="https://incalake.com/galeria/admin/short-slider/PUNO/PUNO-CIUDAD/parque-pino.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</section>
</div>
