<div class="pull-right">
	<a href="<?php echo site_url('categorium/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idcategoria</th>
		<th>Nombre</th>
		<th>Actions</th>
    </tr>
	<?php foreach($categoria as $c){ ?>
    <tr>
		<td><?php echo $c['idcategoria']; ?></td>
		<td><?php echo $c['nombre']; ?></td>
		<td>
            <a href="<?php echo site_url('categorium/edit/'.$c['idcategoria']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('categorium/remove/'.$c['idcategoria']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
