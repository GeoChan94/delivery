<div class="container">
    <h1 class="text-center">Formulario para registro</h1>
    <form action="">
        <div class="row form-group">
            <div class="col-md-6">
                <label for="idNombreCompleto">Tu nombre completo <span>*</span></label>
                <input type="text" name="idNombreCompleto" id="idNombreCompleto" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="dni">DNI<span>*</span></label>
                <input type="text" name="dni" id="dni" class="form-control" maxlength="8">
            </div>

            <div class="col-md-3">
                <label for="idNombreCompleto">Telefono Personal</label>
                <input type="tel" name="idNombreCompleto" id="idNombreCompleto" class="form-control" maxlength="9">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                <label for="nombreEmpresa">Nombre del Negocio <span>*</span> </label>
                <input type="text" name="nombreEmpresa" id="nombreEmpresa" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="direccionEmpresa">Direccion del Negocio <span>*</span> </label>
                <input type="text" name="direccionEmpresa" id="direccionEmpresa" class="form-control">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                <label for="ruc">RUC (Opcional) </label>
                <input type="text" name="ruc" id="ruc" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="ciudad">Ciudad <span>*</span> </label>
                <input type="text" name="ciudad" id="ciudad" class="form-control">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                <span>¿A que categoria pertenece su negocio?</span  >
            </div>
            <div class="form-check col-md-3 ">
                <input type="radio" name="categoria" id="category" class="form-check-input">
                <label for="category" class="form-check-label">CategoryName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="radio" name="categoria" id="category2" class="form-check-input">
                <label for="category2" class="form-check-label">CategoryName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="radio" name="categoria" id="category3" class="form-check-input">
                <label for="category3" class="form-check-label">CategoryName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="radio" name="categoria" id="category4" class="form-check-input">
                <label for="category4" class="form-check-label">CategoryName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="radio" name="categoria" id="category5" class="form-check-input">
                <label for="category5" class="form-check-label">CategoryName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="radio" name="categoria" id="category6" class="form-check-input">
                <label for="category6" class="form-check-label">CategoryName</label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                <span>¿Que Tipo de Productos Ofrece?</span  >
            </div>
            <div class="form-check col-md-3 ">
                <input type="checkbox" name="producto" id="producto1" class="form-check-input">
                <label for="producto1" class="form-check-label">ProductName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="checkbox" name="producto" id="producto2" class="form-check-input">
                <label for="producto2" class="form-check-label">ProductName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="checkbox" name="producto" id="producto3" class="form-check-input">
                <label for="producto3" class="form-check-label">ProductName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="checkbox" name="producto" id="producto4" class="form-check-input">
                <label for="producto4" class="form-check-label">ProductName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="checkbox" name="producto" id="producto5" class="form-check-input">
                <label for="producto5" class="form-check-label">ProductName</label>
            </div>
            <div class="col-md-3 form-check">
                <input type="checkbox" name="producto" id="producto6" class="form-check-input">
                <label for="producto6" class="form-check-label">ProductName</label>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                <label for="urlFb">Url Facebook(Opcional)</label>
                <input type="text" name="urlFb" id="Urlfb" class="form-control">
            </div>
            <div class="col-md-6">
                <label for="urlWeb">Url Pagina Web(Opcional)</label>
                <input type="text" name="urlFb" id="UrlWeb" class="form-control">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md">
                <label for="urlImagen">Imagen Referencial</label>
                <input name="urlImagen" id="urlImagen" type="file" class="form-control-file">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md">
                <label for="descripcion">Descripcion <span>*</span></label>
                <textarea name="descripcion" id="descripcion" rows="10" class="form-control"></textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-md">
                <button type="submit" class="btn btn-success btn-block">Aceptar</button>
            </div>
        </div>

    </form>
</div>