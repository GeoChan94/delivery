<div class="pull-right">
	<a href="<?php echo site_url('distrito/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Iddistrito</th>
		<th>Nombre</th>
		<th>Actions</th>
    </tr>
	<?php foreach($distrito as $d){ ?>
    <tr>
		<td><?php echo $d['iddistrito']; ?></td>
		<td><?php echo $d['nombre']; ?></td>
		<td>
            <a href="<?php echo site_url('distrito/edit/'.$d['iddistrito']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('distrito/remove/'.$d['iddistrito']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
