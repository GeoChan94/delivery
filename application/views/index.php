<div class="container ">
	<div class="row pt-5">
		<div class="col-md-6">
		<h3 class="font-weight-bold text-dark">ENCUENTRA CONTACTOS DE DELIVERY EN PUNO
			<span class="text-danger">DURANTE LA CUARENTENA</span>
		</h3>
		<p class="lead">Conectamos a negocios con permiso para distribuir sus productos con familias que necesitan este servicio para respetar la cuarentena.</p>
		<hr class="my-4">
		<p class="lead">
			<!-- <button type="button" class="btn btn-danger">Buscar Delivery</button> -->
			<a href="<?=base_url();?>buscar" class="btn btn-danger" title="">Buscar Delivery</a>
			<a href="<?=base_url();?>login" class="btn btn-info" title="">Registrar Delivery</a>
			<!-- <a type="button"  class="btn btn-info">Registrar Delivery</button> -->
			<!-- <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a> -->
		</p>
	</div>
	<div class="col-md-6">
		<img src="<?=asset_url()?>/img/fastdelivery.png" alt="" width="100%">
	</div>
	</div>
	
	
</div>