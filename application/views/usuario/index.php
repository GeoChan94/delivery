<div class="pull-right">
	<a href="<?php echo site_url('usuario/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Idusuario</th>
		<th>Password</th>
		<th>Usuario</th>
		<th>Actions</th>
    </tr>
	<?php foreach($usuario as $u){ ?>
    <tr>
		<td><?php echo $u['idusuario']; ?></td>
		<td><?php echo $u['password']; ?></td>
		<td><?php echo $u['usuario']; ?></td>
		<td>
            <a href="<?php echo site_url('usuario/edit/'.$u['idusuario']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('usuario/remove/'.$u['idusuario']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
