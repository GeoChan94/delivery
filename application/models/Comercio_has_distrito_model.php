<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Comercio_has_distrito_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get comercio_has_distrito by idcomercio
     */
    function get_comercio_has_distrito($idcomercio)
    {
        return $this->db->get_where('comercio_has_distrito',array('idcomercio'=>$idcomercio))->row_array();
    }
        
    /*
     * Get all comercio_has_distrito
     */
    function get_all_comercio_has_distrito()
    {
        $this->db->order_by('idcomercio', 'desc');
        return $this->db->get('comercio_has_distrito')->result_array();
    }
        
    /*
     * function to add new comercio_has_distrito
     */
    function add_comercio_has_distrito($params)
    {
        $this->db->insert('comercio_has_distrito',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update comercio_has_distrito
     */
    function update_comercio_has_distrito($idcomercio,$params)
    {
        $this->db->where('idcomercio',$idcomercio);
        return $this->db->update('comercio_has_distrito',$params);
    }
    
    /*
     * function to delete comercio_has_distrito
     */
    function delete_comercio_has_distrito($idcomercio)
    {
        return $this->db->delete('comercio_has_distrito',array('idcomercio'=>$idcomercio));
    }
}
